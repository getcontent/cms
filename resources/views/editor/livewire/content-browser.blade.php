<div>
    <!-- Search and action header -->
    <header class="sticky top-0 bg-white shadow-sm">
        <div class="flex relative z-10 flex-shrink-0 border-b border-gray-200 lg:h-16">

            <button
                class="px-4 text-gray-500 border-r border-gray-200 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 lg:hidden"
                @click.stop="sidebarOpen = true">
                <span class="sr-only">Open sidebar</span>
                <x-heroicon-s-menu-alt-1 class="w-6 h-6"></x-heroicon-s-menu-alt-1>
            </button>

            <div class="flex justify-between px-4 w-full">

                <form class="flex w-full" action="#" method="GET">
                    <label for="search_field" class="sr-only">Search</label>
                    <div class="relative w-full text-gray-400 focus-within:text-gray-600">
                        <div class="flex absolute inset-y-0 left-0 items-center pointer-events-none">
                            <x-heroicon-s-search class="w-5 h-5"></x-heroicon-s-search>
                        </div>
                        <input id="search_field" name="search_field"
                               class="block py-2 pr-3 pl-8 w-full h-full placeholder-gray-500 text-gray-900 bg-transparent border-transparent focus:outline-none focus:ring-0 focus:border-transparent focus:placeholder-gray-400 sm:text-sm"
                               placeholder="Search" type="search">
                    </div>
                </form>

                <div class="flex flex-shrink-0 items-center my-2 space-x-2 sm:my-0">
                    <button type="button"
                            class="inline-flex items-center p-2 text-indigo-600 rounded-full hover:text-indigo-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            title="Create Group">
                        <x-heroicon-o-folder-add class="w-6 h-6"></x-heroicon-o-folder-add>
                        <span class="sr-only">Create Group</span>
                    </button>

                    <button type="button"
                            class="inline-flex items-center p-2 text-indigo-600 rounded-full hover:text-indigo-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            title="Create Document">
                        <x-heroicon-o-pencil-alt class="w-6 h-6"></x-heroicon-o-pencil-alt>
                        <span class="sr-only">Create Document</span>
                    </button>
                </div>

            </div>
        </div>

        <nav class="flex items-center text-sm bg-gray-50 border-b select-none">
            <ol class="flex py-2 px-4 mx-auto w-full max-w-screen-xl h-10">

                <li class="flex">
                    <div class="flex items-center">
                        <button wire:click="changeGroup()" class="flex text-sm font-medium text-gray-500 hover:text-gray-700">
                            <x-heroicon-s-collection class="flex-shrink-0 mr-2 w-5 h-5"></x-heroicon-s-collection>
                            <span class="sr-only sm:not-sr-only">Documents</span>
                        </button>
                    </div>
                </li>

                @if($this->currentGroup)
                    @foreach($this->currentGroup->ancestors as $ancestor)
                        <li class="flex">
                            <div class="flex items-center">
                                <x-heroicon-o-chevron-right class="flex-shrink-0 w-6 text-gray-300" aria-hidden="true"></x-heroicon-o-chevron-right>

                                <button wire:click="changeGroup('{{$ancestor->uuid}}')"
                                        class="text-sm font-medium text-gray-500 hover:text-gray-700">{{$ancestor->name}}</a>
                            </div>
                        </li>
                    @endforeach

                    <li class="flex">
                        <div class="flex items-center">
                            <x-heroicon-o-chevron-right class="flex-shrink-0 w-6 text-gray-300" aria-hidden="true"></x-heroicon-o-chevron-right>
                            <button wire:click="changeGroup('{{$this->currentGroup->uuid}}')"
                                    class="text-sm font-medium text-gray-500 hover:text-gray-700">{{$this->currentGroup->name}}</a>
                        </div>
                    </li>
                @endif
            </ol>
        </nav>
    </header>

    <section class="flex flex-col mx-auto divide-y divide-gray-100 cursor-default select-none"
             x-data="selection()"
    >
        @foreach($groups as $group)
            <button class="flex p-4 cursor-default"
                    :class="{'bg-blue-50': _.includes(selection, 'group:{{$group->uuid}}')}"
                    wire:key="{{$group->uuid}}{{$group->id}}"
                    x-on:click="selectItem('group:{{$group->uuid}}', event)"
                    wire:dblclick="changeGroup('{{ $group->uuid }}')"
            >
                <x-heroicon-s-folder class="mr-2 w-6 h-6 text-blue-300"></x-heroicon-s-folder>
                {{$group->name}}
            </button>
        @endforeach

        @foreach($listing as $document)
            <button class="flex p-4 cursor-default"
                    :class="{'bg-blue-50': _.includes(selection, 'document:{{$document->uuid}}')}"
                    wire:key="{{$document->uuid}}{{$document->id}}"
                    x-on:click="selectItem('document:{{$document->uuid}}', event)"
                    wire:dblclick="openDocument('{{ $document->uuid }}')"
            >
                <x-heroicon-o-document class="mr-2 w-6 h-6 text-blue-300"></x-heroicon-o-document>
                <span class="truncate">{{$document->name}}</span>
            </button>
        @endforeach
    </section>
</div>

@push('scripts')
    <script>
        function selection() {
            return {
                selection: [],
                selectItem: function (uuid, event) {

                    if (!event.metaKey) {
                        return this.selection = [uuid]
                    }

                    if (_.includes(this.selection, uuid)) {
                        _.pull(this.selection, uuid);
                    } else {
                        this.selection.push(uuid)
                    }

                    this.selection = _.uniq(this.selection)
                }
            }
        }
    </script>
@endpush
