<x-html title="GetContent CMS" class="cursor-default select-none">
    <x-slot name="head">
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
        <link rel="stylesheet" href="{{ GetContent::editorAssetUrl('css/app.css') }}?{{ GetContent::version() }}">
        @livewireStyles
    </x-slot>

    <div class="flex overflow-hidden h-screen bg-white"
         x-data="{ sidebarOpen: false }"
         @keydown.window.escape="sidebarOpen = false"
    >
        <!-- Off-canvas menu for mobile, show/hide based on off-canvas menu state. -->
        <div x-show="sidebarOpen" class="lg:hidden">
            <div class="flex fixed inset-0 z-40">
                <div @click="sidebarOpen = false" x-show="sidebarOpen"
                     x-transition:enter="transition-opacity ease-linear duration-300"
                     x-transition:enter-start="opacity-0"
                     x-transition:enter-end="opacity-100"
                     x-transition:leave="transition-opacity ease-linear duration-300"
                     x-transition:leave-start="opacity-100"
                     x-transition:leave-end="opacity-0"
                     class="fixed inset-0"
                     aria-hidden="true">
                    <div class="absolute inset-0 bg-gray-900 opacity-75"></div>
                </div>
                <div x-show="sidebarOpen"
                     x-transition:enter="transition ease-in-out duration-300 transform"
                     x-transition:enter-start="-translate-x-full"
                     x-transition:enter-end="translate-x-0"
                     x-transition:leave="transition ease-in-out duration-300 transform"
                     x-transition:leave-start="translate-x-0"
                     x-transition:leave-end="-translate-x-full"
                     class="flex relative flex-col flex-1 pt-5 w-full max-w-xs bg-white">
                    <div class="absolute top-0 right-0 pt-2 -mr-12">
                        <button x-show="sidebarOpen" @click="sidebarOpen = false"
                                class="flex justify-center items-center ml-1 w-10 h-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                            <span class="sr-only">Close sidebar</span>
                            <x-heroicon-o-x class="w-6 h-6 text-white"></x-heroicon-o-x>
                        </button>
                    </div>
                    <x-GetContent::editor.sidebar></x-GetContent::editor.sidebar>
                </div>
                <div class="flex-shrink-0 w-14" aria-hidden="true">
                    <!-- Dummy element to force sidebar to shrink to fit close icon -->
                </div>
            </div>
        </div>

        <!-- Static sidebar for desktop -->
        <div class="hidden lg:flex lg:flex-shrink-0">
            <div class="flex flex-col pt-5 w-64 bg-gray-100 border-r border-gray-200">
                <x-GetContent::editor.sidebar></x-GetContent::editor.sidebar>
            </div>
        </div>

        <!-- Main column -->
        <div class="flex overflow-hidden flex-col flex-1 w-0">
            <main class="overflow-y-auto relative z-0 flex-1 focus:outline-none" tabindex="0">
                @yield('body')
                {{ $slot ?? null }}
            </main>
        </div>
    </div>


    @livewireScripts
    <script src="{{ GetContent::editorAssetURL('js/app.js') }}"></script>
    @stack('scripts')
</x-html>
