<header class="flex flex-shrink-0 items-center px-4 space-x-2">
    <figure class="p-2 text-indigo-50 bg-indigo-900 rounded-full">
        <x-heroicon-s-collection class="w-5 h-5"></x-heroicon-s-collection>
    </figure>
    <span class="text-lg font-bold tracking-tighter text-indigo-800">
        GetContent
    </span>
</header>

<div class="overflow-y-auto flex-1 mt-5 h-0">
    <nav class="px-2">
        <div class="space-y-1">

            <a href="#"
               class="flex items-center py-2 px-2 text-sm font-medium text-gray-900 bg-gray-200 rounded-md group"
               aria-current="page">
                <x-heroicon-o-collection
                    class="mr-3 w-6 h-6 text-gray-500"></x-heroicon-o-collection>
                Documents
            </a>

            <a href="#"
               class="flex items-center py-2 px-2 text-base font-medium leading-5 text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50 group">
                <x-heroicon-o-folder-open
                    class="mr-3 h-6 text-gray-400 group-hover:text-gray-500 w-66"></x-heroicon-o-folder-open>
                Media
            </a>

        </div>
        <div class="mt-8">
            <h3 class="px-3 text-xs font-semibold tracking-wider text-gray-500 uppercase"
                id="teams-headline">
                Recent Documents
            </h3>
            <div class="mt-1 space-y-1" role="group" aria-labelledby="teams-headline">
                <a href="#"
                   class="flex items-center py-2 px-3 text-base font-medium leading-5 text-gray-600 rounded-md group hover:text-gray-900 hover:bg-gray-50">
                                        <span class="w-2.5 h-2.5 mr-4 bg-indigo-500 rounded-full"
                                              aria-hidden="true"></span>
                    <span class="truncate">
                                            A recently edited document
                                        </span>
                </a>
            </div>
        </div>
    </nav>
</div>

<footer class="sticky bottom-0 p-4 text-xs text-center text-gray-400 border-t transition duration-150 hover:bg-gray-50 hover:underline">
    GetContent {{GetContent::version()}}
    @if(config('app.env') === 'local')
    <span
        class="relative inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-500 text-green-200">
        Local Env
    </span>
    @endif
</footer>
