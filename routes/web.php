<?php

use GetContent\CMS\Http\Controllers\FrontendController;

/**
 * Front-end
 * All front-end website requests go through a single controller method.
 */
Route::any('/{segments?}', FrontendController::class)
    ->where('segments', '^(?!livewire).*')
    ->name('getcontent.site');
