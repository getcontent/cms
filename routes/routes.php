<?php

use GetContent\CMS\Http\Controllers\GetContentEditorController;
use Illuminate\Support\Facades\Route;

ray(config());
if (config('getcontent.editor.enabled')) {
    Route::group(
        ['middleware' => 'web'],
        function () {
            Route::get('editor', GetContentEditorController::class);
        }
    );
}

if (config('getcontent.routes.enabled')) {
    Route::middleware(config('getcontent.routes.middleware', 'web'))
        ->group(__DIR__.'/web.php');
}
