<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGetContentTable extends Migration
{
    public function up()
    {
        Schema::create('GetContentCMS_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->index();
            $table->string('name');
            $table->string('slug')->index();
            $table->string('status')->nullable()->index();
            $table->text('description')->nullable();

            $table->timestamps();
        });
    }
}
