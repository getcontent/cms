<?php

namespace GetContentCMS\GetContent\Tests;

use GetContentCMS\GetContent\Models\Document;

class DocumentTest extends TestCase
{
    /** @test */
    public function generates_model_name_for_new_field()
    {
        $document = new Document(
            [
                'schema' => [
                    ['type' => 'text', 'model' => 'text'],
                    ['type' => 'text', 'model' => 'text2'],
                    ['type' => 'text', 'model' => 'text6'],
                ]
            ]
        );

        $this->assertEquals('text7', $document->nextModelOfType('text'));
    }

    /** @test */
    public function adds_field()
    {
        $document = new Document(
            [
                'schema' => [
                    [
                        'type' => 'text',
                        'model' => 'text1'
                    ],
                ],
                'model' => [
                    'text1' => 'This is some text'
                ]
            ]
        );

        $document->addField(['type' => 'text']);

        $this->assertArrayHasKey('text2', $document->model->toArray());
    }
}
