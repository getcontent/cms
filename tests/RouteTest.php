<?php

namespace GetContentCMS\GetContent\Tests;

class RouteTest extends TestCase
{
    /** @test */
    public function editor_route_is_available()
    {
        $this->get('/editor')
            ->assertStatus(200);
    }
}
