<?php

namespace GetContent\CMS\Facades;

use Illuminate\Support\Facades\Facade;

class GetContent extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'GetContent';
    }
}
