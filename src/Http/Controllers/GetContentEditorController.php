<?php

namespace GetContent\CMS\Http\Controllers;

class GetContentEditorController {

    public function __invoke()
    {
        return view('GetContent::editor.index');
    }
}
