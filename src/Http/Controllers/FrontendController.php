<?php

namespace GetContent\CMS\Http\Controllers;

class FrontendController {

    public function __invoke()
    {
        return view('GetContent::index');
    }
}
