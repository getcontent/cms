<?php

namespace GetContent\CMS\Http\Livewire;

use GetContent\CMS\Models\Document;
use GetContent\CMS\Models\Group;
use Livewire\Component;
use Livewire\WithPagination;

class ContentBrowser extends Component
{
    use WithPagination;

    public $group = null;
    public $groups = [];

    protected $queryString = ['group'];

    public function mount()
    {
        $this->changeGroup(request()->query('group', $this->group));
    }

    public function getCurrentGroupProperty()
    {
        return Group::whereUuid($this->group)->first();
    }

    public function changeGroup($groupUuid = null)
    {
        $this->resetPage();

        $this->group = $groupUuid;
        $this->groups = ($groupUuid ? $this->currentGroup->children() : Group::whereIsRoot())->orderBy('name')->get();
    }

    public function openDocument($documentUuid)
    {
        return redirect()->route('getcontent.document',  ['document' => $documentUuid]);
    }

    public function render()
    {
        return view('GetContent::editor.livewire.content-browser', [
            'listing' => Document::where('group_id', $this->currentGroup->id ?? null)->orderBy('name')->paginate(100)
        ])->layout('GetContent::editor.layout');
    }
}
