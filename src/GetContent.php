<?php

namespace GetContent\CMS;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class GetContent
{
    const PACKAGE = 'getcontent/cms';

    public static function version()
    {
        if (!file_exists(__DIR__ . '/../composer.json')) {
            return 'alpha';
        }

        return data_get(
            json_decode(file_get_contents(__DIR__ . '/../composer.json')),
            'version',
            'alpha'
        );
    }

    public static function isEditorRoute()
    {
        return Str::startsWith(request()->path(), config('getcontent.editor.route'));
    }

    public static function vendorAssetUrl($url = '/')
    {
        return asset(URL::asset('vendor/' . $url));
    }

    public static function editorAssetUrl($url = '/')
    {
        return static::vendorAssetUrl('getcontent/cms/' . $url);
    }

}
