<?php

namespace GetContent\CMS\Models;

use Arr;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed type
 * @property mixed template
 */
class Field extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $attributes = [
        'model' => null,
        'label' => null,
        'template' => null,
        'placeholder' => null,
    ];

    /**
     * Handle template field configuration
     *
     * @param $template
     * @return object|null
     */
    public function getTemplateAttribute($template)
    {
        if (!$template) {
            return null;
        }

        return (object)[
            'replicator' => Arr::get($template, 'replicator', false),
            'schema' => collect(
                Arr::get(
                    $template,
                    'schema',
                    Arr::get($template, 'fields', [])
                )
            )->transform(
                function ($field) {
                    return new Field($field);
                }
            )
        ];
    }

    public function getLabelAttribute($label)
    {
        return $label ?? ucfirst($this->type) . ' field';
    }

    /**
     * Return a list of paths to look for this fields view
     *
     * @return array
     */
    public function getViewsAttribute(): array
    {
        return [
            "fields.{$this->type}",
            "GetContent::components.fields.{$this->type}",
            'fields.unknown',
            'GetContent::components.fields.unknown'
        ];
    }
}
