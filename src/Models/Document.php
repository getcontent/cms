<?php

namespace GetContent\CMS\Models;

use Arr;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Validator;

/**
 * @property mixed group
 * @property mixed schema
 * @property mixed model
 */
class Document extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $attributes = [
        'name' => null,
        'schema' => [],
        'model' => [],
    ];

    protected $casts = [
        'model' => 'collection',
        'content' => 'collection'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Transforms JSON schema into Form models
     *
     * @param $schema
     * @return Collection
     */
    public function getSchemaAttribute($schema): Collection
    {
        if (empty($schema)) {
            return $this->group->schema;
        }

        return collect($schema)->transform(
            function ($field) {
                return new Field($field);
            }
        );
    }

    /**
     * Calculates the next auto model name for the specified field type
     * by pulling out model names with the same type and
     * incrementing the highest iteration
     *
     * @param $type
     * @return string
     */
    public function nextModelOfType($type): string
    {
        $lastOfType = collect($this->schema)
            ->where('type', $type)
            ->pluck('model')
            ->filter(
                function ($item) use ($type) {
                    return Str::startsWith($item, $type);
                }
            )
            ->sort()
            ->last();

        return $type . ((int)filter_var($lastOfType, FILTER_SANITIZE_NUMBER_INT) + 1);
    }

    /**
     * Add a new Field to the document
     *
     * @param $fieldAttributes
     *
     * @throws \Exception
     */
    public function addField($fieldAttributes)
    {
        if (!Arr::has($fieldAttributes, 'model')) {
            // Auto create model key
            $fieldAttributes['model'] = $this->nextModelOfType(Arr::get($fieldAttributes, 'type'));
        }

        $validator = Validator::make(
            $fieldAttributes,
            [
                'type' => 'required',
                'model' => [
                    function ($attribute, $value, $fail) {
                        // needs to be a unique model key
                        if (Arr::exists($this->model, $value)) {
                            $fail("$value is not a unique model key");
                        }
                    },
                ],
            ]
        );

        if ($validator->fails()) {
            throw new \Exception('Unable to add field');
        }

        $this->attributes['schema'][] = $validator->validated();
        $this->model = $this->model->put(
            $validator->validated()['model'],
            null
        );
    }
}
