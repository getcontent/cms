<?php

namespace GetContent\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;

class Group extends Model
{
    use HasFactory;
    use NodeTrait;

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    /**
     * Transforms JSON schema into Form models
     *
     * @param $schema
     * @return Collection
     */
    public function getSchemaAttribute($schema): Collection
    {
        return collect(json_decode($schema, true))->transform(
            function ($field) {
                return new Field($field);
            }
        );
    }
}
