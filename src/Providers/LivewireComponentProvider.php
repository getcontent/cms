<?php

namespace GetContent\CMS\Providers;

use GetContent\CMS\Http\Livewire\ContentBrowser;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

class LivewireComponentProvider extends ServiceProvider
{
    public function boot() {
        Livewire::component('content-browser', ContentBrowser::class);
    }
}
