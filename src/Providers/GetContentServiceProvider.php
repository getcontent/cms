<?php

namespace GetContent\CMS\Providers;

use GetContent\CMS\GetContent;
use Illuminate\Support\ServiceProvider;

class GetContentServiceProvider extends ServiceProvider
{
    protected $root = __DIR__ . '/../..';

    protected $configFiles = ['editor', 'routes',];

    public function boot()
    {
        $this->loadViewsFrom("{$this->root}/resources/views", 'GetContent');
        $this->loadRoutesFrom("{$this->root}/routes/routes.php");

        collect($this->configFiles)->each(function ($config) {
            ray("{$this->root}/config/$config.php");
            $this->mergeConfigFrom("{$this->root}/config/$config.php", "getcontent.$config");
            $this->publishes(["{$this->root}/config/$config.php" => config_path("getcontent/$config.php")], 'getcontent');
        });

        $this->publishes(["{$this->root}/resources/views" => base_path('resources/views/vendor/getcontent')], 'views');
        $this->publishes(["{$this->root}/public" => public_path('vendor/getcontent/cms')], 'public');

        $migrationFileName = 'create_GetContent_table.php';
        if (!$this->migrationFileExists($migrationFileName)) {
            $this->publishes(
                [
                    "{$this->root}/database/migrations/{$migrationFileName}.stub" => database_path(
                        'migrations/' . date('Y_m_d_His', time()) . '_' . $migrationFileName
                    ),
                ],
                'migrations'
            );
        }
    }

    public function register()
    {
        $this->app->register(LivewireComponentProvider::class);

        $this->app->bind(
            'GetContent',
            function ($app) {
                return new GetContent();
            }
        );

        collect($this->configFiles)->each(function ($config) {
            ray("{$this->root}/config/$config.php");
            $this->mergeConfigFrom("{$this->root}/config/$config.php", "getcontent.$config");
        });
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName);
        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName)) {
                return true;
            }
        }

        return false;
    }
}
