<?php

return [
    /*
     * --------------------------------------------------------------------------
     * Enable Routes
     * --------------------------------------------------------------------------
     * GetContent adds its own routes to front-end of your site.
     * Turn this off if you'd rather handle it yourself.
     * */
    'enabled' => true,
];
