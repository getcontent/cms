<?php

return [
    /*
     * --------------------------------------------------------------------------
     * GetContent Editor
     * --------------------------------------------------------------------------
     * Choose if you want the default editor
     * enabled and the route to load it on
     * */
    'enabled' => env('EDITOR_ENABLED', true),
    'route' => env('EDITOR_ROUTE', 'editor'),
];
