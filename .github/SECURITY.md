# Security Policy

If you discover any security related issues, please email code@richstandbrook.dev instead of using the issue tracker.
